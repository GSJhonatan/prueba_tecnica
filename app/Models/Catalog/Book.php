<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $fillable = [
        'status',
        'author',
        'tittle',
        'tittle_page'
    ];

    public function categories()
    {
      return $this->belongsToMany(Category::class);
    }
    public function category()
    {
     return $this->hasOne(Category::class , 'id' , 'category_id');
    }

    // * Scopes
    public function scopeCategory($query, $category_id,$table = "")
    {
        if (!is_null($category_id) && $category_id !== "%" && $category_id !== "")
        {
          return !empty($table) ? $query->where("{$table}.category_id", $category_id): $query->where('category_id', $category_id);

        }

    }
    public function scopeStatus($query, $status,$table = "")
    {
        if (!is_null($status) && $status !== "%" && $status !== "")
        {
          return !empty($table) ? $query->where("{$table}.status", $status): $query->where('status', $status);

        }

    }
  
}
