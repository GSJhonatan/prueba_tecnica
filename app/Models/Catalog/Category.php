<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'status',
        'name'
    ];

    // Scopes
    public function scopeStatus($query, $status,$table = "")
    {
        if (!is_null($status) && $status !== "%" && $status !== "")
        {
          return !empty($table) ? $query->where("{$table}.status", $status): $query->where('status', $status);

        }

    }
}
