<?php

namespace App\Repositories\Catalog;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Catalog\Book;
use Illuminate\Support\Facades\Storage;

class BookRepository
{
    public function index($status_books, $status_catagory)
    {
        return Book::with(['categories:id,name'])
            ->selectRaw('books.id,
                         books.status,
                         books.author,
                         books.tittle,
                         books.tittle_page')
            ->join('book_category as bc', 'bc.book_id', '=', 'books.id')
            ->status($status_books, 'books')
            ->category($status_catagory, 'bc')
            ->distinct()
            ->get();
    }

    public function store($data)
    {
        $book_tittle_page = "";
        $response = (object)[];
        try {


            $book_tittle_page = $this->loadFile('tittle_page', 'book_tittle_page');

            $categories = $data->categories;
            $book = Book::create([
                'status' => 1,
                'tittle' => $data->tittle,
                'author' => $data->author,
                'tittle_page' => $book_tittle_page
            ]);

            $book->categories()->attach($categories);
            $response->success = true;
            $response->data =  $book;
            $response->message = __('msg.data_was_saved');
        } catch (\Throwable $th) {
            $this->deleteFile('book_tittle_page', $book_tittle_page);
            $response->success = false;
            $response->code = $th->getCode();
            $response->line =  $th->getLine();
            $response->file =  $th->getFile();
            $response->message = $th->getMessage();
        }
        return $response;
    }

    public function find($id)
    {
        return  Book::with(['categories:id,name'])->find($id);
    }

    public function update($req, $object)
    {

        $response = (object)[];
        try {
            $book_tittle_page = ($req->hasFile('tittle_page'))
                ?   $this->loadFile('tittle_page', 'book_tittle_page')
                :   $object->tittle_page;

            $object->update([
                'status' => $req->status,
                'author' => $req->author,
                'tittle' => $req->tittle,
                'tittle_page' => $book_tittle_page
            ]);
            $categories = $req->categories;
            $object->categories()->sync($categories);
            $response->success = true;
            $response->data =  $object;
            $response->message = __('msg.data_was_updated');
        } catch (\Throwable $th) {
            $response->success = false;
            $response->code = $th->getCode();
            $response->line =  $th->getLine();
            $response->file =  $th->getFile();
            $response->message = $th->getMessage();
        }
        return $response;
    }
    public static function loadFile($input_file, $disk, $id = null)
    {
        $store = (is_null($id)) ? "" : $id;
        $path = null;
        if (request()->hasFile($input_file)) {
            $file = request()->file($input_file);
            $path = $file->store($store, $disk);
        }
        return $path;
    }

    public static function deleteFile($disk, $picture)
    {
        Storage::disk($disk)->delete('URL', $picture);
        return;
    }
}
