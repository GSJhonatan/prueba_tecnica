<?php

namespace App\Repositories\Catalog;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Catalog\Category;

class CategoryRepository
{
    public function index($status)
    {
        return Category::status($status)->get();
    }

    public function store($data)
    {
        
        $response = (object)[];
        try
        {
            $object = Category::create([
                'name' => $data->name,
                'status' => 1
            ]);
            $response->success = true;
            $response->data =  $object;
            $response->message = __('msg.data_was_saved');
        }
        catch (\Throwable $th)
        {
           
            $response->success = false;
            $response->code = $th->getCode();
            $response->line =  $th->getLine();
            $response->file =  $th->getFile();
            $response->message = $th->getMessage();
        }
        return $response;
    }

    public function find($id)
    {
        return  Category::find($id);
    }

    public function update($req,$object)
    {
        
        $response = (object)[];
        try
        {
            $object->update([
                'name' => $req->name,
                'status' => $req->status
            ]);

            $response->success = true;
            $response->data =  $object;
            $response->message = __('msg.data_was_updated');
        }
        catch (\Throwable $th)
        {
            
            $response->success = false;
            $response->code = $th->getCode();
            $response->line =  $th->getLine();
            $response->file =  $th->getFile();
            $response->message = $th->getMessage();
        }
        return $response;
    }
}