<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;


use App\Models\Catalog\Book;
use App\Repositories\Catalog\BookRepository;
use App\Repositories\Catalog\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{

    public $book_repository;
    public $category_repository;

    public function __construct()
    {
        $this->book_repository = new BookRepository;
        $this->category_repository = new CategoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = $this->category_repository->index(1);
        $books = $this->book_repository->index($request->status_books, $request->status_catagory);

        return view('catalogues.book.index', [
            'books' => $books,
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obj = new Book();
        $categories = $this->category_repository->index(1);
        return view('catalogues.book.create', [
            'obj' => $obj,
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tittle' => ['required', 'string', 'max:150'],
            'author' => ['required', 'string', 'max:150'],
            'tittle_page' => ['required', 'image']
        ]);

        DB::beginTransaction();
        try {
            $response = $this->book_repository->store($request);
            if (!$response->success) {
                DB::rollBack();
                return back()->withInput()->with(
                    'error',
                    __('msg.html_error_save') . $response->code .
                        __('msg.error_message') . $response->message
                );
            }
            DB::commit();
            return redirect()->route('books.index')->with('success', $response->message);
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->withInput()->with(
                'error',
                __('msg.html_error_save') . $th->getCode() .
                    __('msg.error_message') . $th->getMessage()
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Catalog\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->book_repository->find($id);
        $categories = $this->category_repository->index(1);
        return view('catalogues.book.edit', [
            'obj' => $obj,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Catalog\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::beginTransaction();
        $obj = Book::find($id);

        try {

            $response = $this->book_repository->update($request, $obj);
            // dd($response);
            if (!$response->success) {
                DB::rollBack();
                return back()->withInput()->with(
                    'error',
                    __('msg.html_error_save') . $response->code .
                        __('msg.error_message') . $response->message
                );
            }
            DB::commit();

            return redirect()->route('books.index')->with('success', $response->message);
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->withInput()->with(
                'error',
                __('msg.html_error_save') . $th->getCode() .
                    __('msg.error_message') . $th->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Catalog\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
