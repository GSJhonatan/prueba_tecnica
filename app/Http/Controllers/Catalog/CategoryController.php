<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;


use App\Models\Catalog\Category;
use App\Repositories\Catalog\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    public $category_repository;

    public function __construct()
    {
        $this->category_repository = new CategoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = $this->category_repository->index($request->status_catagories);

        return view('catalogues.category.index', [
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obj = new Category();

        // $spaces = $this->space_repository->findSpaceApi($headquarter_id);
        return view('catalogues.category.create', [
            'obj' => $obj
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {


            $category = $this->category_repository->store($request);
            if (!$category->success) {
                DB::rollBack();
                return back()->withInput()->with(
                    'error',
                    __('msg.html_error_save') . $category->code .
                        __('msg.error_message') . $category->message
                );
            }

            DB::commit();

            return redirect()->route('categories.index')->with('success', $category->message);
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->withInput()->with(
                'error',
                __('msg.html_error_save') . $th->getCode() .
                    __('msg.error_message') . $th->getMessage()
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Catalogues\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = $this->category_repository->find($id);
        return view('catalogues.category.edit', [
            'obj' => $obj
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Catalogues\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $obj = Category::find($id);
        DB::beginTransaction();
        try {
            $category = $this->category_repository->update($request, $obj);
            if (!$category->success) {
                DB::rollBack();
                return back()->withInput()->with(
                    'error',
                    __('msg.html_error_save') . $category->code .
                        __('msg.error_message') . $category->message
                );
            }
            DB::commit();
            return redirect()->route('categories.index')->with('success', $category->message);
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->withInput()->with(
                'error',
                __('msg.html_error_save') . $th->getCode() .
                    __('msg.error_message') . $th->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Catalogues\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
