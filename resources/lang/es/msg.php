<?php
return [
        //* Server Response
        "data_was_saved"    =>  "Los datos ingresados han sido guardados satisfactoriamente",
        "data_was_updated"  =>  "Los datos ingresados han sido actualizados satisfactoriamente",
        "data_was_cancelled"=>  "El registro ha sido cancelado satisfactoriamente",
        "data_not_found"    =>  "Lo sentimos, pero el registro al que intenta acceder no figura en nuestros datos",
        'html_error_save'   =>  'Ha ocurrido un imprevisto al intentar guardar el registro.<br> * Código de error: #',
        'unexpected_error'  =>  'Ha ocurrido un imprevisto.<br> * Código de error: #',
        'error_message'     =>  '<br>* Mensaje de error:  ',
        

        //buttons
        'save'              => 'Guardar',
        'cancel'            => 'Cancelar',
        'update'            => 'Actualizar',
        'return'            => 'Regresar',
        'back'              => 'Volver',
        'undo'              => 'Deshacer',
        'filter'            => 'Filtrar',
        'id'                => 'Id',
        'noRecordsFound'    => 'No se encontraron Registros',
        'status'            => 'Estado',
        'registry'          => 'Registró',
        'registered'        => 'Registrado',
        'editedBy'          => 'Editó',
        'updated'           => 'Actualizado',
        'active'            => 'Activo',
        'inactive'          => 'Inactivo',
        'pending'           => 'Pendiente',
        'select_one'         => 'Seleccione...',
];