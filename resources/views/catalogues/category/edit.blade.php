@extends('layouts.app')

@push('css_after')
    <link rel="stylesheet" href="{{ asset('/js/plugins/flatpickr/themes/material_green.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@section('page-title')
    {{ __('Editar Categoría') }}
@endsection

@section('block-header')
@if(session()->has('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <p class="mb-0">
        {{session('error')}}
    </p>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
@endsection

@section('block-content')
<form id="crud_form" method="POST" action="{{ route('categories.update', $obj) }}" class="crud_form">
    @method('PATCH')
    @include('catalogues.category.form' )
</form>
@endsection

@push('js_after')
    <script src="{{ asset('/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/flatpickr/l10n/es.js') }}"></script>
    <script src="{{ asset('/js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/select2/js/i18n/es.js') }}"></script>
@endpush