@extends('layouts.app')

@push('css_before')
<link rel="stylesheet" href="{{ asset('js/plugins/datatables-bs5/css/dataTables.bootstrap5.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs5/buttons.bootstrap5.min.css') }}">
<link rel="stylesheet" href="{{ asset('/js/plugins/magnific-popup/magnific-popup.css') }}">
@endpush

@section('page-title')
{{ __('Lista de categorías') }}
@endsection

@section('buttons')
{{-- {{ $util->generaBotones('parametros_generales', 'holidays.create', null) }} --}}
<a type="button" href="{{route('categories.create')}}" class="btn btn-success">Registrar Categoría</a>
@endsection

@section('block-extra')
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible" role="alert">
    <p class="mb-0">
        {{session('success')}}
    </p>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif


<form method="GET" action="" role="search" class="form-search">
    <div class="block block-rounded">
        <div class="block-content block-content-full">
            <div class="js-summernote-air">
                @csrf
                @php
                $status = request('status_catagories');
                @endphp
                <div class="block-header block-header-default text-center">
                    <div class="form-group  form-group-status col-md-3 col-sm-12" id="form-group-status">
                        <label for="status">{{ __('Estado') }}</label>
                        <select name="status_catagories" class="form-select" id="filter-index-status_catagories">
                            <option value='%' {{ ( $status == '%' ) ? 'selected' : '' }}>{{ __('Todos') }}</option>
                            <option value='1' {{ ( $status == '1' ) ? 'selected' : '' }}>{{ __('Activos') }}</option>
                            <option value='0' {{ ( $status == '0' ) ? 'selected' : '' }}>{{ __('Inactivos') }}</option>
                        </select>
                    </div>

                    <div class="block-options">
                        <button id="filterBtn" type="submit" class="btn btn-primary"> {{ __('Filtrar') }} </button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('block-header')
@endsection

@section('block-content')
<!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized at the top of this page -->
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover table-vcenter js-dataTable-full table-sm">
        <thead class="thead-light">
            <tr>
                <th></th>
                <th class="text-center">{{ __('ID') }}</th>
                <th class="text-center">{{ __('Estado') }}</th>
                <th class="text-center">{{ __('Nombre') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($categories as $item)
            <tr>
        
                <td class="text-center">
                    <a class="extra_data btn btn-sm btn-primary" href="{{route('categories.edit',$item->id)}}" title="{{"Editar id $item->id"}}">
                        <i class="nav-main-link-icon si si-note"></i>
                    </a>
                </td>

                <td class="text-center">{{$item->id}}</td>
                <td class="text-center">{{$item->status == 1 ? 'Activo' : 'Inactivo' }}</td>
                <td class="text-center">{{$item->name }}</td>

            </tr>
            @empty
            <tr>
                <th scope="row" colspan="15" class="text-center">{{ trans('msg.noRecordsFound') }}</th>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection

@push('js_after')

<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.flash.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('js/plugins/magnific-popup/jquery.magnific-popup.js') }}"></script>

<script src="{{ mix('js/pages/tables_datatables.js') }}"></script>
@endpush
