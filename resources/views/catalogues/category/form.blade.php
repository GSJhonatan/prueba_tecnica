@csrf
<div class="form-group row mb-4">
    @if (!is_null($obj->id))
    <div class="form-group col-lg-3 col-md-4 col-sm-12">
        <label for="status">{{ __('Estado') }} <span class="text-danger">*</span></label><br>
        <select id="status" name="status" class="form-select @error('status') is-invalid @enderror" required>
            <option value="" {{ (old( "status",$obj->status) === null ) ? 'selected' :'' }}>{{ __('Seleccione') }}</option>
            <option value="1" {{ (old( "status",$obj->status) == "1" )  ? 'selected' :'' }}> {{ __('Activo') }}</option>
            <option value="0" {{ (old( "status",$obj->status) == "0" )  ? 'selected' :'' }}> {{ __('Inactivo') }}</option>
        </select>
        <span class="invalid-feedback" role="alert">
            <strong>@error('status'){{ $message }}@enderror</strong>
        </span>
    </div>
    @endif

    <div class="form-group col-lg-3 col-md-4 col-sm-12">
        <label for="name">{{ __('Nombre') }} <span class="text-danger">*</span></label><br>
        <input id="name" name="name" type="text" class="maxlength form-control @error('name') form-control-alt is-invalid @enderror"
         placeholder="{{ __('Nombre') }}" value="{{ old('name', $obj->name) }}" maxlength="50" required>
        <span class="invalid-feedback" role="alert">
            <strong>@error('name') {{ $message }} @enderror</strong>
        </span>
    </div>

</div>
<div class="form-group row mb-4">
    <div class="col-md-12">
        <button id="send" type="submit" class="btn btn-primary btn-md float-right">
            <i class="{{ (is_null($obj->id))  ? 'fas fa-save' : 'fas fa-sync-alt' }} mr-1"></i>
            {{ (is_null($obj->id)) ? __('Guardar') : __('Actualizar') }}
        </button>
        <a href="{{ route('categories.index') }}" class="btn btn-light btn-undo btn-md mr-2 float-right">
            <i class="fas fa-undo-alt mr-1"></i>{{ __('Regresar') }}
        </a>
    </div>
</div>
