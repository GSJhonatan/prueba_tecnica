@extends('layouts.app')

@push('css_after')
    <link rel="stylesheet" href="{{ asset('/js/plugins/flatpickr/themes/material_green.css') }}">
@endpush

@section('page-title')
    {{ __('Registrar Categoría') }}
@endsection

@section('block-header')
@if(session()->has('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <p class="mb-0">
        {{session('error')}}
    </p>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
@endsection

@section('block-content')
<form id="crud_form" method="POST" action="{{ route('categories.store')}} " enctype="multipart/form-data">
    @include('catalogues.category.form')
</form>
@endsection

@push('js_after')
    <script src="{{ asset('/js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/js/plugins/flatpickr/l10n/es.js') }}"></script>
@endpush