@csrf
<div class="form-group row mb-4">
    @if (!is_null($obj->id))
    <div class="form-group col-lg-3 col-md-4 col-sm-12">
        <label for="status">{{ __('Estado') }} <span class="text-danger">*</span></label><br>
        <select id="status" name="status" class="form-select @error('status') is-invalid @enderror" required>
            <option value="" {{ (old( "status",$obj->status) === null ) ? 'selected' :'' }}>{{ __('Seleccione') }}</option>
            <option value="1" {{ (old( "status",$obj->status) == "1" )  ? 'selected' :'' }}> {{ __('Activo') }}</option>
            <option value="0" {{ (old( "status",$obj->status) == "0" )  ? 'selected' :'' }}> {{ __('Inactivo') }}</option>
        </select>
        <span class="invalid-feedback" role="alert">
            <strong>@error('status'){{ $message }}@enderror</strong>
        </span>
    </div>
    @endif


    <div class="form-group  mb-4 col-lg-3 col-md-4 col-sm-12">
        <label for="category_id">{{ __('Categorias') }}<span class="text-danger">*</span></label><br>
        <select class="form-control js-select2 form-select select2-multiple" id="catagories" name="categories[]" data-placeholder="Seleccione.." multiple="multiple" required>
            <option value="">{{__('Seleccione..')}}</option>
            @foreach ($categories as $category)
            <option {{ collect(old('categories',$obj->categories->pluck('id')))->contains($category->id) ? 'selected' :'' }} value="{{$category->id}}">{{$category->name}}</option>
            @endforeach

        </select>


    </div>

    <div class="form-group col-lg-3 col-md-4 col-sm-12">
        <label for="tittle">{{ __('Titúlo') }} <span class="text-danger">*</span></label><br>
        <input id="tittle" name="tittle" type="text" class="maxlength form-control @error('tittle') form-control-alt is-invalid @enderror" placeholder="{{ __('Nombre') }}" value="{{ old('tittle', $obj->tittle) }}" maxlength="50" required>
        <span class="invalid-feedback" role="alert">
            <strong>@error('tittle') {{ $message }} @enderror</strong>
        </span>
    </div>
    <div class="form-group col-lg-3 col-md-4 col-sm-12">
        <label for="author">{{ __('Author') }} <span class="text-danger">*</span></label><br>
        <input id="author" name="author" type="text" class="maxlength form-control @error('author') form-control-alt is-invalid @enderror" placeholder="{{ __('Nombre') }}" value="{{ old('author', $obj->author) }}" maxlength="50" required>
        <span class="invalid-feedback" role="alert">
            <strong>@error('author') {{ $message }} @enderror</strong>
        </span>
    </div>
    <div class="form-group col-lg-3 col-md-4 col-sm-12">
        <label for="tittle_page">{{ __('Portada') }} <span class="text-danger">*</span></label><br>
        <div class="custom-file">
            <input id="tittle_page" name="tittle_page" type="file" class="custom-file-input form-control @error('tittle_page') form-control-alt is-invalid @enderror" data-toggle="custom-file-input">
        </div>

        <span class="invalid-feedback" role="alert">
            <strong>@error('tittle_page'){{ $message }}@enderror</strong>
        </span>
    </div>
    <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
        <label for="tittle_page">{{ __('Portada') }}</label><br>
        <a target="_blank" class="img-link img-link-zoom-in img-thumb img-lightbox magnific-pic" href="{{ (is_null($obj->tittle_page)) ? '/media/avatars/avatar0.jpg' : Storage::disk('book_tittle_page')->url($obj->tittle_page) }}">
            <img class="img-fluid" style="width: 6rem;" src="{{ (is_null($obj->tittle_page)) ? '/media/avatars/avatar0.jpg' : Storage::disk('book_tittle_page')->url($obj->tittle_page) }}" alt="Logo">
        </a>
    </div>

</div>
<div class="form-group row mb-4">
    <div class="col-md-12">
        <button id="send" type="submit" class="btn btn-primary btn-md float-right">
            <i class="{{ (is_null($obj->id))  ? 'fas fa-save' : 'fas fa-sync-alt' }} mr-1"></i>
            {{ (is_null($obj->id)) ? __('Guardar') : __('Actualizar') }}
        </button>
        <a href="{{ route('categories.index') }}" class="btn btn-light btn-undo btn-md mr-2 float-right">
            <i class="fas fa-undo-alt mr-1"></i>{{ __('Regresar') }}
        </a>
    </div>
</div>
