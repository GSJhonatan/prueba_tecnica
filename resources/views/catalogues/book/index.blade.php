@extends('layouts.app')

@push('css_before')
<link rel="stylesheet" href="{{ asset('js/plugins/datatables-bs5/css/dataTables.bootstrap5.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/plugins/datatables-buttons-bs5/css/buttons.bootstrap5.min.css') }}">
<link rel="stylesheet" href="{{ asset('/js/plugins/magnific-popup/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('/js/plugins/datatables-responsive-bs5/css/responsive.bootstrap5.min.css') }}">
@endpush

@section('page-title')
{{ __('Lista de Libros') }}
@endsection

@section('buttons')
<a type="button" href="{{route('books.create')}}" class="btn btn-success">Registrar Libro</a>
@endsection

@section('block-extra')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible" role="alert">
    <p class="mb-0">
        {{session('success')}}
    </p>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif




<div class="block block-rounded">
    <div class="block-content block-content-full">
            @csrf
            @php
            $status = request('status_books');
            $category = request('status_catagory');
            @endphp
            <form method="GET" action="" role="search" class="form-search">
                <div class="block-header block-header-default text-center">
                    <div class="form-group mb-4  form-group-status col-md-3 col-sm-12" id="form-group-status">
                        <label for="status">{{ __('Estado') }}</label>
                        <select name="status_books" class="form-select" id="filter-index-status-books">
                            <option value='%' {{ ( $status == '%' ) ? 'selected' : '' }}>{{ __('Todos') }}</option>
                            <option value='1' {{ ( $status == '1' ) ? 'selected' : '' }}>{{ __('Activos') }}</option>
                            <option value='0' {{ ( $status == '0' ) ? 'selected' : '' }}>{{ __('Inactivos') }}</option>
                        </select>
                    </div>
                    <div class="form-group mb-4 col-md-3 col-sm-12" id="form-group-status">
                        <label for="status">{{ __('Categorias') }}</label>
                        <select name="status_catagory" class="form-select" id="filter-index-status-books">
                            <option value='%' {{ ( $category == '%' ) ? 'selected' : '' }}>{{ __('Todos') }}</option>
                            @foreach ($categories as $item)
                            <option value="{{$item->id}}" {{ ( $category == $item->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="block-options">
                        <button id="filterBtn" type="submit" class="btn btn-primary"> {{ __('Filtrar') }} </button>

                    </div>

                </div>
            </form>
    </div>
</div>

@endsection

@section('block-header')

@endsection

@section('block-content')
<!-- DataTables init on table by adding .js-dataTable-buttons class, functionality is initialized at the top of this page -->
<div class="table-responsive">
    <table class="table table-bordered table-striped table-vcenter js-dataTable-full dataTable no-footer table-sm">
        <thead class="thead-light">
            <tr>
                <th ></th>
                <th class="text-center">{{ __('ID') }}</th>
                <th class="text-center">{{ __('Estado') }}</th>
                <th class="text-center">{{ __('Titulo') }}</th>
                <th class="text-center">{{ __('Author') }}</th>
                <th class="text-center">{{ __('Portada') }}</th>
                <th class="text-center">{{ __('Categorías') }}</th>

            </tr>
        </thead>
        <tbody>
            @forelse ($books as $item)
            <tr>

                <td class="text-center">
                    <a class="btn btn-sm btn-primary" href="{{route('books.edit',$item->id)}}" title="{{"Editar id $item->id"}}">
                        <i class="nav-main-link-icon si si-note"></i>
                    </a>
                </td>

                <td class="text-center">{{$item->id}}</td>
                <td class="text-center">{{$item->status == 1 ? 'Activo' : 'Inactivo' }}</td>
                <td class="text-center">{{$item->tittle }}</td>
                <td class="text-center">{{$item->author }}</td>
                <td class="text-center">
                    @php
                    $partes = explode(".", $item->tittle_page);
                    $extension = end($partes);
                    @endphp

                    @if($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'jpeg')

                    <div class="animated fadeIn">
                        <a target="_blank" class="img-link img-link-zoom-in img-thumb img-lightbox magnific-pic" href="{{ (is_null($item->tittle_page)) ? '/media/avatars/avatar0.jpg' : Storage::disk('book_tittle_page')->url($item->tittle_page) }}">
                            <img class="img-fluid" style="width: 2.5rem;" src="{{ (is_null($item->tittle_page)) ? '/media/avatars/avatar0.jpg' : Storage::disk('book_tittle_page')->url($item->tittle_page) }}" alt="">
                        </a>
                    </div>
                    @else
                    <a class="btn btn-sm" href="{{ Storage::disk('book_tittle_page')->url($item->tittle_page) }}" target="_blank">
                        <i class="fas fa-file-alt" style="font-size: 1.5em"></i>
                    </a>
                    @endif
                </td>
                <td class="text-center">{{$item->categories->pluck('name')->implode(', ') }}</td>

            </tr>
            @empty
            <tr>
                <th scope="row" colspan="7" class="text-center">{{ __('Sin registros') }}</th>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection

@push('js_after')


<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.flash.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables-buttons/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('js/plugins/magnific-popup/jquery.magnific-popup.js') }}"></script>

<script src="{{ mix('js/pages/tables_datatables.js') }}"></script>
@endpush
