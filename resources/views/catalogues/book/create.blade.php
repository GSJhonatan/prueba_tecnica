@extends('layouts.app')

@push('css_after')
<link rel="stylesheet" href="{{ asset('/js/plugins/flatpickr/themes/material_green.css') }}">
<link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@section('page-title')
{{ __('Registrar Libro') }}
@endsection

@section('block-header')
@if(session()->has('error'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <p class="mb-0">
        {{session('error')}}
    </p>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
@endsection

@section('block-content')
<form id="crud_form" method="POST" action="{{ route('books.store')}} " enctype="multipart/form-data">
    @include('catalogues.book.form')
</form>
@endsection

@push('js_after')
<script src="{{ asset('/js/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('/js/plugins/select2/js/i18n/es.js') }}"></script>
<!-- Page JS Helpers (Flatpickr + BS Datepicker + BS Maxlength + Select2 + Masked Inputs + Ion Range Slider + BS Colorpicker plugins) -->
{{-- <script>One.helpersOnLoad(['jq-select2']);</script> --}}
<script>
    const mSelect = $(".select2-multiple");

    mSelect.select2({
        language: "es"
        , theme: 'bootstrap4'
    , });

</script>
<script>
    One.helpersOnLoad(['jq-select2']);

</script>

@endpush
