<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

  <title>Prueba Tercnica</title>

  <meta name="description" content="OneUI - Bootstrap 5 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
  <meta name="author" content="pixelcave">
  <meta name="robots" content="noindex, nofollow">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Icons -->
  <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
  <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

  <!-- Fonts and Styles -->
  @yield('css_before')
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
  <link rel="stylesheet" id="css-main" href="{{ mix('/css/oneui.css') }}">
  <link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/js/plugins/select2/css/select2-bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/js/plugins/datatables-bs5/css/dataTables.bootstrap5.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/js/plugins/datatables-buttons-bs5/css/buttons.bootstrap5.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/js/plugins/datatables-responsive-bs5/css/responsive.bootstrap5.min.css')}}">
  @yield('css_after')

  <!-- Scripts -->
  <script>
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
  </script>
</head>

<body>

  <div id="page-container" class="sidebar-o enable-page-overlay sidebar-dark side-scroll page-header-fixed main-content-narrow">


    <nav id="sidebar" aria-label="Main Navigation">
      <!-- Side Header -->
      <div class="content-header">
        <!-- Logo -->
       
        <!-- END Logo -->

        <!-- Extra -->
        <div>
          <!-- Dark Mode -->
          
          <!-- Options -->
          <div class="dropdown d-inline-block ms-1">
            
          
          </div>
          <!-- END Options -->
        </div>
        <!-- END Extra -->
      </div>
      <!-- END Side Header -->

      <!-- Sidebar Scrolling -->
      <div class="js-sidebar-scroll">
        <!-- Side Navigation -->
        <div class="content-side">
          <ul class="nav-main">
            <li class="nav-main-heading">Catalogos</li>
            <li class="nav-main-item open">
              <a class="nav-main-link" href="{{route('categories.index')}}">
                <i class="nav-main-link-icon si si-globe"></i>
                <span class="nav-main-link-name">Categorias</span>
              </a>
              <a class="nav-main-link" href="{{route('books.index')}}">
                <i class="nav-main-link-icon si si-globe"></i>
                <span class="nav-main-link-name">Libros</span>
              </a>
            </li>
          </ul>
        </div>
        <!-- END Side Navigation -->
      </div>
      <!-- END Sidebar Scrolling -->
    </nav>
    <!-- END Sidebar -->

    <!-- Header -->
    <header id="page-header">
      <!-- Header Content -->
      <div class="content-header">
        <!-- Left Section -->
        <div class="d-flex align-items-center">
        <!-- Right Section -->
        <div class="d-flex align-items-center">

        </div>
        <!-- END Right Section -->
      </div>
      <!-- END Header Content -->

      <!-- Header Search -->
      <div id="page-header-search" class="overlay-header bg-body-extra-light">
        <div class="content-header">
          <form class="w-100" action="/dashboard" method="POST">
            @csrf
            <div class="input-group">
              <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
              <button type="button" class="btn btn-alt-danger" data-toggle="layout" data-action="header_search_off">
                <i class="fa fa-fw fa-times-circle"></i>
              </button>
              <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
            </div>
          </form>
        </div>
      </div>
      <!-- END Header Search -->

      <!-- Header Loader -->
      <!-- Please check out the Loaders page under Components category to see examples of showing/hiding it -->
      <div id="page-header-loader" class="overlay-header bg-body-extra-light">
        <div class="content-header">
          <div class="w-100 text-center">
            <i class="fa fa-fw fa-circle-notch fa-spin"></i>
          </div>
        </div>
      </div>
      <!-- END Header Loader -->
    </header>
    <!-- END Header -->

    <!-- Main Container -->
         <!-- Main Container -->
         <main id="main-container">
          <div id="page-loader" class="show"></div>
          <!-- Hero -->
          <div class="bg-body-light">
              <div class="content content-full">
                  <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                      <h1 id="tituloCatalogo" class="flex-sm-fill h3 my-2">@yield('page-title')@yield('page-title2')</h1>
                      <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">@yield('buttons')</nav>
                      @yield('extras')
                  </div>
              </div>
          </div>
          <!-- END Hero -->
          <!-- Page Content -->
          <div class="content">
              @yield('block-extra')
              <!-- Dynamic Table with Export Buttons -->
              <div class="block">
                  <div class="block-header">
                      @yield('block-header')
                  </div>
                  <div class="block-content block-content-full">
                      @yield('block-content')
                  </div>
              </div>
          </div>
          <!-- END Page Content -->
      </main>
      <!-- END Main Container -->
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="bg-body-light">
      <div class="content py-3">
        <div class="row fs-sm">
        </div>
      </div>
    </footer>
    <!-- END Footer -->

    <div class="modal fade" id="one-modal-apps" tabindex="-1" role="dialog" aria-labelledby="one-modal-apps" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-md" role="document">
          <div class="modal-content" style="width: auto;">
              <div class="block block-rounded block-themed block-transparent mb-0">
                  <div id="modal-header" class="block-header bg-primary-dark">
                      <h3 id="modal-title" class="block-title">Apps</h3>
                      <div class="block-options">
                          <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                              <i class="si si-close"></i>
                          </button>
                      </div>
                  </div>
                  <div class="block-content block-content-full">
                      <div id="modal-body" class="row gutters-tiny text-center">
                          <div class="col-6">
                              <!-- CRM -->
                              <a class="block block-rounded block-link-shadow bg-body" href="javascript:void(0)">
                                  <div class="block-content text-center">
                                      <i class="si si-speedometer fa-2x text-primary"></i>
                                      <p class="font-w600 font-size-sm mt-2 mb-3">
                                          CRM
                                      </p>
                                  </div>
                              </a>
                              <!-- END CRM -->
                          </div>
                          <div class="col-6">
                              <!-- Products -->
                              <a class="block block-rounded block-link-shadow bg-body" href="javascript:void(0)">
                                  <div class="block-content text-center">
                                      <i class="si si-rocket fa-2x text-primary"></i>
                                      <p class="font-w600 font-size-sm mt-2 mb-3">
                                          Products
                                      </p>
                                  </div>
                              </a>
                              <!-- END Products -->
                          </div>
                          <div class="col-6">
                              <!-- Sales -->
                              <a class="block block-rounded block-link-shadow bg-body mb-0" href="javascript:void(0)">
                                  <div class="block-content text-center">
                                      <i class="si si-plane fa-2x text-primary"></i>
                                      <p class="font-w600 font-size-sm mt-2 mb-3">
                                          Sales
                                      </p>
                                  </div>
                              </a>
                              <!-- END Sales -->
                          </div>
                          <div class="col-6">
                              <!-- Payments -->
                              <a class="block block-rounded block-link-shadow bg-body mb-0" href="javascript:void(0)">
                                  <div class="block-content text-center">
                                      <i class="si si-wallet fa-2x text-primary"></i>
                                      <p class="font-w600 font-size-sm mt-2 mb-3">
                                          Payments
                                      </p>
                                  </div>
                              </a>
                              <!-- END Payments -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  </div>
  <!-- END Page Container -->

  <!-- OneUI Core JS -->
  <script src="{{ mix('js/oneui.app.js') }}"></script>
  <script src="{{asset('/js/lib/jquery.min.js') }}"></script>
  
  <script src="{{asset('/js/plugins/select2/js/select2.full.min.js') }}"></script>
  <script src="{{asset('/js/plugins/select2/js/i18n/es.js') }}"></script>
  <!-- Laravel Scaffolding JS -->
  <!-- <script src="{{ mix('/js/laravel.app.js') }}"></script> -->
  <script src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-bs5/js/dataTables.bootstrap5.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-responsive-bs5/js/responsive.bootstrap5.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-buttons/dataTables.buttons.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-buttons-bs5/js/buttons.bootstrap5.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-buttons-jszip/jszip.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-buttons-pdfmake/pdfmake.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-buttons-pdfmake/vfs_fonts.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-buttons/buttons.print.min.js')}}"></script>
  <script src="{{ asset('/js/plugins/datatables-buttons/buttons.html5.min.js')}}"></script>
  @stack('js_after')
</body>

</html>
