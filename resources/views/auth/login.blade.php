@extends('layouts.authentication')
@section('content')
    <!-- Page Content -->
    <div class="bg-image" style="background-image: url('media/photos/photo28@2x.jpg');">
        <div class="row g-0 bg-primary-dark-op">
            <div class="hero-static d-flex align-items-center">
                <div class="content">
                    <div class="row justify-content-center push">
                        <div class="col-md-8 col-lg-6 col-xl-4">
                            <!-- Sign In Block -->
                            <div class="block block-rounded mb-0">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Iniciar Sesión</h3>
                                    <div class="block-options">
                                        @if (Route::has('password.request'))
                                            <a class="btn-block-option fs-sm"  href="{{route('password.request')}}">¿Olvidaste tu contraseña?</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="p-sm-3 px-lg-4 px-xxl-5 py-lg-5">
                                        <h1 class="h2 mb-1">Sistema Base</h1>
                                        <p class="fw-medium text-muted">
                                            Bienvenido, por favor inicie sesión.
                                        </p>

                                        <!-- Sign In Form -->
                                        <!-- jQuery Validation (.js-validation-signin class is initialized in js/pages/op_auth_signin.min.js which was auto compiled from _js/pages/op_auth_signin.js) -->
                                        <!-- For more info and examples you can check out https://github.com/jzaefferer/jquery-validation -->
                                        <form class="js-validation-signin" action="be_pages_auth_all.html" method="POST">
                                            <div class="py-3">
                                                <div class="mb-4">
                                                    <input type="text" class="form-control form-control-alt form-control-lg" id="login-username" name="login-username" placeholder="Correo">
                                                </div>
                                                <div class="mb-4">
                                                    <input type="password" class="form-control form-control-alt form-control-lg" id="login-password" name="login-password" placeholder="Constraseña">
                                                </div>
                                                <div class="mb-4">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="" id="login-remember" name="login-remember">
                                                        <label class="form-check-label" for="login-remember">Recordar Sesión</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-md-6 col-xl-5">
                                                    <button type="submit" class="btn w-100 btn-alt-primary">
                                                        <i class="fa fa-fw fa-sign-in-alt me-1 opacity-50"></i> Ingresar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END Sign In Form -->
                                    </div>
                                </div>
                            </div>
                            <!-- END Sign In Block -->
                        </div>
                    </div>
                    <div class="content-full  text-center text-white-50 mb-0">
                        <strong>Jhonatan Garcia</strong> &copy; <span data-toggle="year-copy"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection
