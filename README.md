# Documentacion Arrancar el sistema

Tabla de contenido
-----------------
* [Requerimientos](#requerimientos)
* [Instalación](#instalación)



Requerimientos
------------

* PHP ^7.4
* Composer ^2.0
* Nodejs ^14.16.0

Instalación
------------

Para poder relizar una instalación limpia para el desarrollador del sistema se deben seguir las siguientes instrucciones
Se ejecutan en el presente orden los siguientes comandos desde una consola
```
composer install


```

Ejecutado los comandos anteriores se debe proceder a la creación del archivo `.env`, creado e a partir del archivo `.env.example` debe ser copiado y los valores de las variables deben ser sustituidos por las que se manejarán en el ambiente donde se esté instalando el sistema, seguido al mismo se procede a la ejecución del siguiente comando:

```
php artisan key:generate
```

Para que se pueda realizar la carga de archivos se debe crear un enlace simbólico en `/public/storage` desde `/storage/app/public` que será donde sean almacenados los archivos cargados en cada uno de los distintos catalogos que hagan uso de la herramienta, para ello se ejecuta el siguiente comando:

```
php artisan storage:link
```

Dichos archivos para poder ser almacenados deberá ser creado un driver para darle la directiva a laravel y este pueda almacenar el o los archivos que sean cargados. Esto se configura en `/config/filesystems.php`, dentro de este archivo debemos modificar uno de los discos para crear nuestro como se muestra a continuación
```php

    'disks' => [
        'book_tittle_page' => [
            'driver' => 'local',
            'root' => storage_path('app/public/books'),
            'url' => env('APP_URL').'/storage/books',
            'visibility' => 'public',
        ],
    ]
```
**'disks'** es el o la directiva que declara al o los discos del sistema **'empleado-profile'** es el nombre del disco dentro de este se declaran los siguientes datos, **'driver'** en este se declara donde y de que manera serán almacenados los archivos a subir en este caso será local lo cual indica que todos los archivos serán cargados dentro de las carpetas de laravel, **'root'** aquí es declarada la ruta de donde serán almacenados los archivos partiendo que el directorio se refiere al directorio `/storage`, **'url'** es donde es declarada la ruta que será mostrada en el navegador al acceder de manera pública a un archivo esto mantendrá los archivos publicamente accesibles en un directorio que puede ser fácilmente compartido a través de despliegues,  **'visibility'** indica que el archivo puede ser accesado de manera publica siempre que tenga acceso a la url.

Para terminar la instalación será necesario haber configurado el archivo `.env` los datos de conexión a una base de datos ya creada y vacía y ejecutar los siguientes comandos:

```console
    php artisan migrate

```
