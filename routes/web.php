<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController as Home;
use App\Http\Controllers\Catalog\CategoryController as Category;
use App\Http\Controllers\Catalog\BookController as Book;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return redirect('home');});
Route::get('/home', [Home::class, 'index'])->name('home');
Route::resource('/categories', Category::class);
Route::resource('/books', Book::class);

// Auth::routes();


